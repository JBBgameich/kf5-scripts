#!/usr/bin/env python3

import requests
import sys
import json
import tarfile
import io

from ruamel import yaml

if (len(sys.argv) < 4):
    print("usage: ./download-kf5-android.py QT_VERSION PLATFORM DIRECTORY")
    print("Platform can be one of Android or WindowsMSVC")
    exit(1)

QT_VERSION=sys.argv[1]
PLATFORM=sys.argv[2]
DIRECTORY=sys.argv[3]

DOWNLOAD_BASEURL = "https://build-artifacts.kde.org/production/{}Qt{}/".format(PLATFORM, QT_VERSION)
MANIFEST_URL = DOWNLOAD_BASEURL + "/manifest.yaml"


if __name__ == "__main__":
    yaml = yaml.YAML()
    manifest = yaml.load(requests.get(MANIFEST_URL).text)

    for framework in manifest:
        if "Applications" in framework and "stable" in framework:
            download_url = DOWNLOAD_BASEURL + framework + manifest[framework]["contentsSuffix"]

            print("Retrieving", framework)
            response = requests.get(download_url)

            print("Extracting", framework)
            file_like_object = io.BytesIO(response.content)
            tar = tarfile.open(fileobj=file_like_object)
            tar.extractall(DIRECTORY)
            tar.close()
