#!/usr/bin/env bash
set -e

QT_VERSION=${1:-5.14}
PLATFORM=${2:-Android}

if [ -z ${PLATFORM} ] || [ -z {QT_VERSION} ]; then
    echo "Required argument missing"
    exit 1
fi
mkdir -p install-${PLATFORM}
./download-kf5.py ${QT_VERSION} ${PLATFORM} install-${PLATFORM}
cd install-${PLATFORM}
7z a ../KF5-${PLATFORM}-${QT_VERSION}-full.7z *
cd ..
